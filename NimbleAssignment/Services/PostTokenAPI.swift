//
//  GetTokenAPI.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 02/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import Foundation

class PostTokenAPI{
    
    func makeAPICall(fromViewController : MainViewController,
                     url : String,
                     bodyParameters : [String : String]){
        
        //Making an Post API call Request
        APICallHelper.shared.makePOSTApiCall(url: url,
                                            bodyParameters: bodyParameters,
                                            headersParameters: [:],
                                            showLoader: true)
        { (response, error) in
            
            // If api response is nil then will show an alert with error
            guard let apiResponse = response else{
                UiUtillity.shared.showAlert(title: "Error", message: (error?.localizedDescription)!, logMessage: "Error In API", fromController: fromViewController)
                return
            }
            
            //Passing the api response to the UIViewController
            fromViewController.fetchTokenAPIResponse(response: apiResponse)
        }
    }
}

