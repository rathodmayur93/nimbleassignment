//
//  GetSurveyAPI.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 02/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import Foundation

class GetSurveyAPI{
    
    func makeAPICall(fromViewController : MainViewController,
                     url : String,
                     bodyParameters : [String : String],
                     showLoader : Bool = true){
        
        
        //Making an Get API call Request
        APICallHelper.shared.makeGetApiCall(url: url,
                                            bodyParameters: bodyParameters,
                                            showLoader: showLoader,
                                            headerParameters: [:])
        { (response, error) in
            
            // If api response is nil then will show an alert with error
            guard let apiResponse = response else{
                
                UiUtillity.shared.showAlert(title: "Error", message: (error?.localizedDescription)!, logMessage: "Error In API", fromController: fromViewController)
                return
            }
            
            //Parsing the json response to the login api response handler since the response is same for login and facebook login api
            fromViewController.fetchSurveyAPIResponse(response: apiResponse)
        }
    }
}
