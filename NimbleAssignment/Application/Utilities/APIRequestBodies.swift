//
//  APIRequestBodies.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 02/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import Foundation

class APIRequestBodies{
    
    static let shared = APIRequestBodies()
    
    //MARK:- Token API Call Request Body
    func fetchTokenRequestBody() -> [String : String]{
        
        let parameters : [String : String] = ["grant_type" : "password",
                                           "username" : Constant.username,
                                           "password" : Constant.passowrd]
        
        return parameters
    }
    
    //MARK:- Survey API Call Parameters
    func fetchSurveyParameters(pageNumber : String, token : String) -> [String : String]{
        
        let parameters : [String : String] = ["page" : pageNumber,
                                              "per_page" : Constant.surveyLimitPerPage,
                                              "access_token" : token]
        
        return parameters
    }
}
