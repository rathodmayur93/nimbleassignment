//
//  ConfigurationFile.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 02/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import Foundation

class ConfigurationFile {
    
    static var shared : ConfigurationFile = ConfigurationFile()
    
    lazy var environment: Environment = {
        if let configuration = Bundle.main.object(forInfoDictionaryKey: "Configuration") as? String {
            if configuration.range(of: "Debug") != nil {
                return Environment.Staging
            }else if (configuration.range(of: "Release") != nil){
                return Environment.Production
            }
        }
        return Environment.Staging
    }()
}

enum Environment: String {
    case Staging        = "staging"
    case Production     = "production"
    
    var baseUrl : String{
        switch self {
            case .Staging        : return "https://nimble-survey-api.herokuapp.com" //Staging base url
            case .Production     : return "https://nimble-survey-api.herokuapp.com" // Production base url
        }
    }
}
