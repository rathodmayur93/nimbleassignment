//
//  UiUtillity.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 02/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import Foundation
import UIKit                // For Accessig the UI Element for showing loader
import SystemConfiguration  // For Checking the internet connection
import SwiftMessages

class UiUtillity{
    
    //Crating static instance of the class
    static let shared       = UiUtillity()
    
    //Required Views For Showing Loader
    let spinningActivityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let container: UIView           = UIView()
    
    
    //MARK:- Convert Hex To UIColor
    /**
     - This function will convert the hex color code string value to the UIColor
     - Parameter hex: Hex Color Code String Value
     - Returns : It will return the hex color code UIColor
    */
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //MARK:- Show Indicator Loader while making an api call
    /**
     - This function will help us to show the loader
     */
    func showIndicatorLoader(){
        
        if(isConnectedToNetwork()){
            
            //This will show the activity indicator next to the wifi symbol or network sybmol on iphone
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let window                      = UIApplication.shared.keyWindow
            container.frame                 = UIScreen.main.bounds
            container.backgroundColor       = UIColor(hue: 0/360, saturation: 0/100, brightness: 0/100, alpha: 0.4)
            
            let loadingView: UIView         = UIView()
            loadingView.frame               = CGRect(x: 0, y: 0, width: 80, height: 80)
            loadingView.center              = container.center
            loadingView.backgroundColor     = UiUtillity.shared.hexStringToUIColor(hex: Colors.placeholderText)
            loadingView.clipsToBounds       = true
            loadingView.layer.cornerRadius  = 40
            
            spinningActivityIndicator.frame                         = CGRect(x: 0, y: 0, width: 40, height: 40)
            spinningActivityIndicator.hidesWhenStopped              = true
            spinningActivityIndicator.style    = UIActivityIndicatorView.Style.whiteLarge
            spinningActivityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2,
                                                       y: loadingView.frame.size.height / 2)
            loadingView.addSubview(spinningActivityIndicator)
            container.addSubview(loadingView)
            window?.addSubview(container)
            spinningActivityIndicator.startAnimating()
        }
    }
    
    //MARK:- Hide Loader
    /**
     - This function will hide the loader if there is loader showing on the screen
    */
    func hideIndicatorLoader(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        spinningActivityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    //MARK:- Show Alert
    /**
     - This function will help us to show alert to the user
    */
    func showAlert(title: String, message: String, logMessage: String, fromController controller: UIViewController){
        
        OperationQueue.main.addOperation {
            
            let alertController = UIAlertController(title: title,
                                                    message: message,
                                                    preferredStyle: UIAlertController.Style.alert)
            
            alertController.addAction(UIAlertAction(title: "Okay",
                                                    style: UIAlertAction.Style.default,handler: nil))
            
            controller.present(alertController, animated: true, completion: nil)
            print("Error Message :- \(logMessage)")
        }
    }
    
    //MARK:- This function will push the viewController over the other view controller
    func pushControllerNavigation(identifierName : String, storyboardName : String, fromController controller : UIViewController){
        
        let mainStoryboard          = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        let vc : UIViewController   = mainStoryboard.instantiateViewController(withIdentifier: identifierName) as UIViewController
        vc.modalPresentationStyle   = .custom
        vc.modalTransitionStyle     = .crossDissolve
        controller.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Store In User Default
    /**
     - This function will help to store the key value into the user default
     - Parameter value: Value which we need to store into the user default
     - Parameter key: Value which we need to store as key reference against the value
    */
    func writeData(value : Any, key: String){
        
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    //MARK:- Read From User Default
    /**
     - This function will help us to read the value from the user defaults
     - Parameter key: Name of key whose value we need to read from the user default
     - Returns : It will return the string value which we fetched from user default using the key
    */
    func readData(key : String) -> String{
        
        let defaults = UserDefaults.standard
        
        if let value = defaults.string(forKey: key){
            return value
        }else{
            return ""
        }
        
    }
    
    //MARK:- Show SnackBar
    func showErrorSnackbarMessage(message : String){
        
        var config       = SwiftMessages.Config()
        let snackView    = MessageView.viewFromNib(layout: .cardView)
        
        // Slide up from the bottom.
        config.presentationStyle = .top
        
        //Set the duration of snackbar
        config.duration = .seconds(seconds: 5.0)
        
        // Theme message elements with the warning style.
        snackView.configureTheme(.info)
        
        // Enable the interactive pan-to-hide gesture.
        config.interactiveHide = false
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        //snackView.configureContent(title: "Oops..", body: message)
        snackView.configureContent(title: "Oops..", body: message, iconText: iconText)
        snackView.button?.isHidden = true
        snackView.bodyLabel?.font  = UIFont.boldSystemFont(ofSize: 12.0)
        //snackView.configureContent(title: "Error", body: message, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        snackView.layoutMargins = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        snackView.layer.cornerRadius    = 10.0
        
        // Show the message.
        SwiftMessages.show(config: config, view: snackView)
    }
    
    //MARK:- Check whether internet connection is available or not
    /**
     - This function will check whether iphone is having the active internet connection or not
     */
    func isConnectedToNetwork() -> Bool {
        var zeroAddress                 = sockaddr_in()
        zeroAddress.sin_len             = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family          = sa_family_t(AF_INET)
        
        let defaultRouteReachability    = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        
        let isReachable         = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection     = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
    }
}
