//
//  APICallHelper.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 01/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import Foundation
import Alamofire

class APICallHelper{
    
    static let shared = APICallHelper()
    
    func makePOSTApiCall(url:String,
                         bodyParameters : [String : Any],
                         headersParameters : [String : String],
                         showLoader : Bool = true,
                         completion : @escaping (Any?, Error?) -> ()){
        
        //MARK: Show indicator loader while making an api call
        if(showLoader){
            UiUtillity.shared.showIndicatorLoader()
        }
        
        print("=======================  API URL =========================== \(url) \n")
        //Making an POST API Request to alamofire
        Alamofire.request(url, method: .post, parameters: bodyParameters, encoding: JSONEncoding.default, headers: headersParameters).responseJSON { (response) in
            
            //MARK: Hide indicator loader once fetching data from api is done
            UiUtillity.shared.hideIndicatorLoader()
            
            //MARK: Switch case to handle success and failure response of the api
            switch response.result{
                
            //Successfully made an api call
            case .success(let JSON):
                print("======================= API Response ====================== \(JSON)")
                completion(JSON, nil)
                
            // Face some issue while executing the api call
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
    func makeGetApiCall(url : String,
                        bodyParameters : [String : Any],
                        showLoader : Bool = true,
                        headerParameters : [String : String],
                        completion : @escaping (Any?, Error?) -> ()){
        
        //If condition will check showLoader value and display the loader
        if(showLoader){
            //MARK: Show indicator loader while making an api call
            UiUtillity.shared.showIndicatorLoader()
        }
        
        print("=======================  API URL =========================== \n \(url) \n")
        
        //Making an GET API Request to alamofire
        
        Alamofire.request(url,
                          method: .get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers : nil).responseJSON
            { (response) in
            
            //MARK: Hide indicator loader once fetching data from api is done
            UiUtillity.shared.hideIndicatorLoader()
            
            //MARK: Switch case to handle success and failure response of the api
            switch response.result{
                
            //Successfully made an api call
            case .success(let JSON):
                print("======================= API Response ====================== \n \(JSON)")
                completion(JSON, nil)
                
            // Face some issue while executing the api call
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
}
