//
//  APIEndPoints.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 02/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import Foundation

struct APIEndPoints {
    
    static let fetchToken = "/oauth/token"      // Post API Request
    static let survey     = "/surveys.json?"    // Get API Request
}
