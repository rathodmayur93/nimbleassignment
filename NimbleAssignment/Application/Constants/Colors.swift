//
//  Colors.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 02/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import Foundation

struct Colors{
    
    static let placeholderText          = "#757575"  // Loader Background Color
    static let backgroundColor          = "#141414"  // ViewController Background Color
}
