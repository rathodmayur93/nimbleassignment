//
//  Constant.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 02/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import Foundation

struct Constant{
    
    //MARK:- Credentitals
    static let username = "carlos@nimbl3.com"
    static let passowrd = "antikera"
    
    //MARK:- User Default Keys
    static let accessToken = "AccessTokenKey"
    
    //MARK:- API Constants
    static let surveyLimitPerPage = "10"
    
    //MARK:- Storyboard
    static let storyBoardName = "Main"
    
    
}
