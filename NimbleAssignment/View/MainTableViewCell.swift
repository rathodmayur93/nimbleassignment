//
//  MainTableViewCell.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 03/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import UIKit

protocol TakeSurveyDelegate {
    func takeSurveyDelegate(cell : MainTableViewCell)
}

class MainTableViewCell: UITableViewCell {

    @IBOutlet weak var coverImageView       : UIImageView!
    @IBOutlet weak var overlayView          : UIView!
    @IBOutlet weak var titleLabel           : UILabel!
    @IBOutlet weak var descriptionLabel     : UILabel!
    @IBOutlet weak var surveyBT             : UIButton!
    
    public var delegate : TakeSurveyDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setCellUI()
    }
    
    func setCellUI(){
        
        self.selectionStyle             = .none
        
        surveyBT.backgroundColor        = UIColor.red
        surveyBT.layer.cornerRadius     = 20.0
        surveyBT.layer.masksToBounds    = true
    }
    
    //Set the data to respective fields
    func setupCell(surveyModel : FetchSurveyModel){
        
        coverImageView.loadImageUsingUrlString(urlString: surveyModel.coverImage)
        overlayView.backgroundColor     = UIColor.clear.withAlphaComponent(0.7)
        titleLabel.text                 = surveyModel.title
        descriptionLabel.text           = surveyModel.description
    }
    
    //Survey Button Action
    @IBAction func surveyButtonAction(_ sender: Any) {
        delegate?.takeSurveyDelegate(cell: self)
    }
    
}
