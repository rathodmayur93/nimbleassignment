//
//  AllExtension.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 02/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension String {
    
    func addingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }
}


extension Dictionary {
    
    /// Build string representation of HTTP parameter dictionary of keys and objects
    /// :returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped
    
    func stringFromHttpParameters() -> String {
        let parameterArray              = self.map { (key, value) -> String in
           
            let percentEscapedKey       = (key as! String).addingPercentEncodingForURLQueryValue()!
            let percentEscapedValue     = String(describing : value).addingPercentEncodingForURLQueryValue()!
            
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joined(separator: "&")
    }
}

extension UIImageView{
    
    func loadImageUsingUrlString(urlString : String){
        
        print("Load Image \(urlString)")
        //Converting the imageURL string into the URL
        let fetchImageURL = URL(string: urlString)
        
        //Creating the placeHolder image and setting it to the imageView
        let placeHolderImageView    = UIImage(named: "noimageList")
        self.image                  = placeHolderImageView
        
        self.sd_setImage(with: fetchImageURL, placeholderImage: placeHolderImageView!, options: SDWebImageOptions.retryFailed) { (image, imageError, imageCacheType, url) in
            
            if(imageError != nil){
                self.image = placeHolderImageView
            }
        }
    }
}

extension UIPageControl {
    
    func customPageControl(dotFillColor:UIColor, dotBorderColor:UIColor, dotBorderWidth:CGFloat) {
        for (pageIndex, dotView) in self.subviews.enumerated() {
            if self.currentPage == pageIndex {
                print("Selected")
                dotView.backgroundColor     = dotFillColor
                dotView.layer.cornerRadius  = dotView.frame.size.height / 2
                dotView.layer.borderWidth   = 0.0
            }else{
                dotView.backgroundColor     = .clear
                dotView.layer.cornerRadius  = dotView.frame.size.height / 2
                dotView.layer.borderColor   = dotBorderColor.cgColor
                dotView.layer.borderWidth   = dotBorderWidth
            }
        }
    }
    
}
