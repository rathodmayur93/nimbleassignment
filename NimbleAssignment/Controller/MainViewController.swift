//
//  MainViewController.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 03/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftMessages

class MainViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    //MARK:- Variabels
    fileprivate var surveyList          = [FetchSurveyModel]()
    fileprivate var reusableIdentifier  = "Cell"
    fileprivate var pageIndex           = 1
    fileprivate var isPaginating        = false
    fileprivate var isPaginationDone    = false
    fileprivate var currentCellIndex    = 0
    
    //Keychain Variables
    let service = "myService"
    let account = "myAccount"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        fetchUserTokenAPICall()
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    //MARK:- UI Functions
    
    func setUI(){
        // Setting the tableview background color and separator style
        setupTableView()
    }
    
    //MARK: Setup Tableview & Its Properties
    /**
     - Setting the tableview background color and separator style
    */
    func setupTableView(){
        
        tableView.backgroundColor           = UiUtillity.shared.hexStringToUIColor(hex: Colors.backgroundColor)
        tableView.separatorStyle            = .none
        tableView.estimatedRowHeight        = 275.0
        tableView.isPagingEnabled           = true
        tableView.rowHeight                 = UITableView.automaticDimension
    }
    
    //MARK:- Configure PageControl
    func configurePageControl() {
        
        self.pageControl.isHidden                       = false
        self.pageControl.numberOfPages                  = surveyList.count
        self.pageControl.currentPage                    = 0
        
        //Making PageControl Vertical
        let angle                                       = CGFloat.pi/2
        let transform                                   = CGAffineTransform(rotationAngle: angle)
        let scale                                       = CGAffineTransform(scaleX: 1.5, y: 1.5)
        self.pageControl.transform                      = scale.concatenating(transform)
        
        //Customizing Page Control
        pageControl.customPageControl(dotFillColor: .red, dotBorderColor: .gray, dotBorderWidth: 3)
    }
    
    //MARK:- Survey Pagination
    func paginateSurveys(){
        
        //Making pagination flag true
        isPaginating = true
        
        //Increament the page number by 1
        pageIndex += 1
        
        //Making the api call to fetch new page surveys
        fetchSurveyAPICall()
    }
    
    
    //MARK:- Navigation
    /**
     - Navigate the user to the questionViewController where user will give feedback
    */
    func navigateToQuestionVC(title : String){
        
        /**
        UiUtillity.shared.pushControllerNavigation(identifierName: "QuestionViewController",
                                                   storyboardName: Constant.storyBoardName,
                                                   fromController: self)
        */
        
        let mainStoryboard                  = UIStoryboard(name: Constant.storyBoardName, bundle: Bundle.main)
        if let vc : QuestionViewController  = mainStoryboard.instantiateViewController(withIdentifier: "QuestionViewController") as? QuestionViewController{
        
            vc.navigationTitle          = title
            vc.modalPresentationStyle   = .custom
            vc.modalTransitionStyle     = .crossDissolve
        
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    //MARK:- IBActions
    
    //MARK: Reload Bar Item Action
    @IBAction func reloadBTAction(_ sender: Any) {
        
        /**
         - If the pagination is done then when user click on refresh icon scroll back to top
         - Else make an api call to fetch the remaining survey list
        */
        if(isPaginationDone){
            
            tableView.scrollToRow(at: IndexPath(row: 0, section: 0),
                              at: .top,
                              animated: true)
            pageControl.currentPage = 0
            UiUtillity.shared.showErrorSnackbarMessage(message: "You have already loaded all surveys.")
            
        }else{
            
            paginateSurveys()
        }
    }
    
    //MARK:- API Calls
    
    //MARK: Fetch token api call
    /**
     - This api call will fetch the user token
     - Token will helpful for making further api calls
     */
    func fetchUserTokenAPICall(){
        
        //Creating fetch token api url
        let apiUrl = ConfigurationFile.shared.environment.baseUrl + APIEndPoints.fetchToken
        
        //Fetch token request body
        let requestBody = APIRequestBodies.shared.fetchTokenRequestBody()
        
        //Creating object of the fetchTokenAPICall
        let fetchTokenAPICall = PostTokenAPI()
        fetchTokenAPICall.makeAPICall(fromViewController: self,
                                      url: apiUrl,
                                      bodyParameters: requestBody)
    }
    
    //MARK:- Fetch Survey List API Call
    /**
     - This function will make an POST Api Request to fetch the list of surveys
     - Will update the pageViewController depending on the api response
     */
    func fetchSurveyAPICall(){
        
        //Fetching the Survey API Url
        let apiUrl = ConfigurationFile.shared.environment.baseUrl + APIEndPoints.survey
        
        //Fetching token from keychain
        let tokenKeychain = KeychainService.loadToken(service: service, account: account)
        
        //Fetch Survey API Parameters
        let paramRequest = APIRequestBodies.shared.fetchSurveyParameters(pageNumber: "\(pageIndex)",
                                                                         token: tokenKeychain ?? "")
        
        //Convert the pararms into the http encoded formated string
        let pararmeterEncodedString = paramRequest.stringFromHttpParameters()
        
        //Appending the pararmeterEncodedString with API Url as final requesting URL
        let actualAPIUrl = apiUrl + pararmeterEncodedString
        
        //Creating the object of GetSurveyAPI class
        let getSurveyAPICall = GetSurveyAPI()
        getSurveyAPICall.makeAPICall(fromViewController: self,
                                     url: actualAPIUrl,
                                     bodyParameters: [:],
                                     showLoader: true)
        
    }
    
    //MARK:- API Responses
    
    //MARK: Fetch token api response handler
    func fetchTokenAPIResponse(response : Any){
        print("%%%%%%%%%%%%%%%% TOKEN API RESPONSE %%%%%%%%%%%%%%%%%% \n \(JSON(response))")
        
        //Parsing the Response to the FetchTokenModel
        let fetchTokenModel = FetchTokenModel(response: response)
        
        guard let accessTokenValue = fetchTokenModel.accessToken else { return }
        UiUtillity.shared.writeData(value: accessTokenValue, key: Constant.accessToken)
        
        let token = KeychainService.loadToken(service: service, account: account)
        print("Token \(token)")
        if(token == nil){
            KeychainService.saveToken(service: service, account: account, data: accessTokenValue)
        }else{
            KeychainService.updateToken(service: service, account: account, data: accessTokenValue)
        }
        
        //Fetching the survey api call
        fetchSurveyAPICall()
        
    }
    
    //MARK:- Fetch Survey API Response
    func fetchSurveyAPIResponse(response : Any){
        print("%%%%%%%%%%%%%%%% SURVEY API RESPONSE %%%%%%%%%%%%%%%%%% \n \(JSON(response))")
        
        let jsonResponse     = JSON(response)
        
        /**
         - This if condition will check that if json response is empty then make the **isPaginationDone** flag true
         - After the **isPaginationDone** flag is set true no further pagination will occur
        */
        if(jsonResponse.isEmpty){
            print("Stop Pagination")
            isPaginationDone = true
            isPaginating     = false
        }
        
        /**
         - Loop through the json response and parse it to the **FetchSurveyModel**
         - append the **FetchSurveyModel** to the **surveyList**
        */
        for i in 0..<jsonResponse.count{
            let fetchSurveyModel = FetchSurveyModel(response: jsonResponse.arrayValue[i])
            surveyList.append(fetchSurveyModel)
        }
        
        print("Survey Result \(surveyList.count)")
        if(isPaginationDone){
            tableView.scrollToRow(at: IndexPath(row: currentCellIndex, section: 0),
                                  at: .top,
                                  animated: false)
        }else{
            print("Reloading TableView")
            
            let contentOffset = self.tableView.contentOffset
            self.tableView.reloadData()
            self.tableView.layoutIfNeeded()
            self.tableView.setContentOffset(contentOffset, animated: false)
        }
        configurePageControl()
    }
}

extension MainViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(surveyList.count != 0){
            return surveyList.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: reusableIdentifier) as? MainTableViewCell{
            cell.setupCell(surveyModel: surveyList[indexPath.row])
            cell.delegate = self
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Did Select \(indexPath.row)")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let statusBarHeight         = UIApplication.shared.statusBarFrame.size.height
        let navigationBarHeight     = self.navigationController?.navigationBar.frame.height ?? 0.0
        let topBarHeight            = statusBarHeight + navigationBarHeight
        print("Cell Row Height \(self.view.frame.height - topBarHeight)")
        return self.view.frame.height - topBarHeight
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let statusBarHeight         = UIApplication.shared.statusBarFrame.size.height
        let navigationBarHeight     = self.navigationController?.navigationBar.frame.height ?? 0.0
        let topBarHeight            = statusBarHeight + navigationBarHeight
        print("Estimate Cell Row Height \(self.view.frame.height - topBarHeight)")
        return self.view.frame.height - topBarHeight
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //Changing
        currentCellIndex        = tableView.indexPathsForVisibleRows?.first?.row ?? 0
        
        
        if(indexPath.row == (surveyList.count - 3) && !isPaginationDone){
            paginateSurveys()
        }
    }
}

//MARK:- TakeSurveyDelegate Method
/**
 - This method contain the action when user clicks on the take survey button
*/
extension MainViewController : TakeSurveyDelegate{
    
    func takeSurveyDelegate(cell: MainTableViewCell) {
        
        if let indexPath = tableView.indexPath(for: cell){
            navigateToQuestionVC(title: surveyList[indexPath.row].title)
        }
        
    }
}

//MARK:- ScrollView Delegate Methods
extension MainViewController : UIScrollViewDelegate{
    
    /**
     - When user finished the scrolling this method will get invoked
    */
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        /**
         - In this will get which cell rect is getting visible
         - once we have the visible point will fetch the indexPath of that particular cell
        */
        let visibleRect         = CGRect(origin: tableView.contentOffset, size: tableView.bounds.size)
        let visiblePoint        = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath    = tableView.indexPathForRow(at: visiblePoint)
        
        /**
         - Will update the pageControl currentPage value.
        */
        pageControl.currentPage = visibleIndexPath?.row ?? 0
        pageControl.customPageControl(dotFillColor: .red, dotBorderColor: .gray, dotBorderWidth: 3)
        
    }
}
