//
//  QuestionViewController.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 07/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {

    public var navigationTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    func setUI(){
        //Setting background color
        view.backgroundColor = UiUtillity.shared.hexStringToUIColor(hex: Colors.backgroundColor)
        //Setting title of the navigation bar
        self.title           = navigationTitle
    }
}
