//
//  FetchTokenModel.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 02/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import Foundation
import SwiftyJSON

class FetchTokenModel{
    
    var accessToken : String?
    var tokenType   : String?
    var expiresIn   : Int?
    var createdAt   : Int?
    
    
    required init(response : Any) {
        
        //Converting the response of type Any to JSON
        let responseJSON = JSON(response)
        
        //Fetching the accessToken Value
        if let accessTokenValue = responseJSON["access_token"].string{
            self.accessToken = accessTokenValue
        }
        
        //Fetching the token type Value
        if let tokenTypeValue = responseJSON["token_type"].string{
            self.tokenType = tokenTypeValue
        }
        
        //Fetching the expiry Value
        if let expiresInValue = responseJSON["expires_in"].int{
            self.expiresIn = expiresInValue
        }
        
        //Fetching the created at Value
        if let createdAtValue = responseJSON["created_at"].int{
            self.createdAt = createdAtValue
        }
    }
}
