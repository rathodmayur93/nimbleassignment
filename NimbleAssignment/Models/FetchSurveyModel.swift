//
//  FetchSurveyModel.swift
//  NimbleAssignment
//
//  Created by Mayur Rathod on 02/04/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import Foundation
import SwiftyJSON

class FetchSurveyModel{
    
    var activeAt          : String!
    var description       : String!
    var title             : String!
    var coverImage        : String!
    var createdAt         : String!
    var tagList           : String!
    var type              : String!
    var theme             = [Theme]()
    var questions         = [Questions]()

    required init(response : JSON) {
        
        if let activeAtValue = response["active_at"].string{
            self.activeAt = activeAtValue
        }
        
        if let descriptionValue = response["description"].string{
            self.description = descriptionValue
        }
        
        if let titleValue = response["title"].string{
            self.title = titleValue
        }
        
        if let coverImageValue = response["cover_image_url"].string{
            self.coverImage = coverImageValue
        }
        
        if let createdAtValue = response["created_at"].string{
            self.createdAt = createdAtValue
        }
        
        if let tagValue = response["tag_list"].string{
            self.tagList = tagValue
        }
        
        if let typeValue = response["type"].string{
            self.type = typeValue
        }
        
        //Parsing the question json array to Question Model Class
        let questionJSON = response["questions"]
        for i in 0..<questionJSON.count{
            
            let questionModel = Questions(response: questionJSON.arrayValue[i])
            self.questions.append(questionModel)
        }
        
        //Parsing the theme json to Theme Model Class
        let themeJSON = response["theme"]
        let themeModel = Theme(response: themeJSON)
        self.theme.append(themeModel)
    }
    
}

class Theme{
    
    var activeColor         : String!
    var colorQuestion       : String!
    var colorAnswerNormal   : String!
    var colorInactive       : String!
    var colorAnswerInactive : String!
    
    required init(response : JSON) {
        
        if let activeColorValue = response["color_active"].string{
            self.activeColor = activeColorValue
        }
        
        if let colorQuestionValue = response["color_question"].string{
            self.colorQuestion = colorQuestionValue
        }
        
        if let colorAnswerValue = response["color_answer_normal"].string{
            self.colorAnswerNormal = colorAnswerValue
        }
        
        if let colorInactiveValue = response["color_inactive"].string{
            self.colorInactive = colorInactiveValue
        }
        
        if let colorAnswerInactiveValue = response["color_answer_inactive"].string{
            self.colorAnswerInactive = colorAnswerInactiveValue
        }
        
    }
    
}

class Questions{
    
    var coverImageURL       : String!
    var helpText            : String!
    var questionText        : String!
    var displayOrder        : Int!
    var coverImageOpacity   : Float!
    var imageUrl            : String!
    var displayType         : String!
    var pick                : String!
    var answers             = [Answers]()
    
    required init(response : JSON) {
        
        if let coverImage = response["cover_image_url"].string{
            self.coverImageURL = coverImage
        }
        
        if let helpTextValue = response["help_text"].string{
            self.helpText = helpTextValue
        }
        
        if let questionTextValue = response["text"].string{
            self.questionText = questionTextValue
        }
        
        if let displayOrderValue = response["display_order"].int{
            self.displayOrder = displayOrderValue
        }
        
        if let coverImageOpacityValue = response["cover_image_opacity"].float{
            self.coverImageOpacity = coverImageOpacityValue
        }
        
        if let imageUrlValue = response["image_url"].string{
            self.imageUrl = imageUrlValue
        }
        
        if let displayTypeValue = response["display_type"].string{
            self.displayType = displayTypeValue
        }
        
        if let pickValue = response["pick"].string{
            self.pick = pickValue
        }
        
        let answerJSON = response["answers"]
        
        for i in 0..<answerJSON.count{
            
            let answerModel = Answers(response: answerJSON.arrayValue[i])
            self.answers.append(answerModel)
        }
        
    }
}

class Answers{
    
    var score           : Int!
    var text            : String!
    var displayOrder    : Int!
    
    required init(response : JSON) {
        if let scoreValue = response["score"].int{
            self.score = scoreValue
        }
        
        if let answerTextValue = response["text"].string{
            self.text = answerTextValue
        }
        
        if let displayOrderValue = response["display_order"].int{
            self.displayOrder = displayOrderValue
        }
    }
}
