//
//  NimbleAssignmentTests.swift
//  NimbleAssignmentTests
//
//  Created by Mayur Rathod on 31/03/19.
//  Copyright © 2019 Mayur. All rights reserved.
//

import XCTest
import Alamofire
@testable import NimbleAssignment

class NimbleAssignmentTests: XCTestCase {

    var mainVC = MainViewController()
    var sessionUnderTest: URLSession!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        sessionUnderTest = URLSession(configuration: URLSessionConfiguration.default)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sessionUnderTest = nil
        super.tearDown()
    }
    /**
     - Unit test case for testing token api
    */
    func testTokenGetHTTPStatusCode200(){
        //Creating fetch token api url
        let apiUrl = ConfigurationFile.shared.environment.baseUrl + APIEndPoints.fetchToken
        
        //Fetch token request body
        let requestBody = APIRequestBodies.shared.fetchTokenRequestBody()
        
        Alamofire.request(apiUrl,
                          method: .post,
                          parameters: requestBody,
                          encoding: JSONEncoding.default,
                          headers: [:]).responseJSON { (response) in
            
            //MARK: Hide indicator loader once fetching data from api is done
            UiUtillity.shared.hideIndicatorLoader()
            
            let promise = self.expectation(description: "Status code: 200")
            //MARK: Switch case to handle success and failure response of the api
            switch response.result{
                
            //Successfully made an api call
            case .success(let JSON):
                promise.fulfill()
            // Face some issue while executing the api call
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
                return
            }
            self.waitForExpectations(timeout: 25, handler: nil)
        }
    }

    // Asynchronous test: success fast, failure slow
    /**
     - Unit testing for survey api
    */
    func testSurveyGetsHTTPStatusCode200() {
        // given
        //Fetching the Survey API Url
        let apiUrl = ConfigurationFile.shared.environment.baseUrl + APIEndPoints.survey
        
        //Fetch Survey API Parameters
        let paramRequest = APIRequestBodies.shared.fetchSurveyParameters(pageNumber: "\(0)")
        
        //Convert the pararms into the http encoded formated string
        let pararmeterEncodedString = paramRequest.stringFromHttpParameters()
        
        //Appending the pararmeterEncodedString with API Url as final requesting URL
        let actualAPIUrl = apiUrl + pararmeterEncodedString
        
        let url = URL(string: actualAPIUrl)
        // 1
        let promise = expectation(description: "Status code: 200")
        
        // Making an api call by making URL session
        let dataTask = sessionUnderTest.dataTask(with: url!) { data, response, error in
            // then
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                    // 2
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        // 3
        waitForExpectations(timeout: 25, handler: nil)
    }
}
